import { Component, OnInit, ViewChild } from '@angular/core';
import { CsvReaderService } from '../services/csvReaderService';
import { NgForm } from '@angular/forms';
import { Http, Response } from '@angular/http';


@Component({
  selector: 'app-csvreader',
  templateUrl: './csvreader.component.html',
  styleUrls: ['./csvreader.component.css']
})
export class CsvreaderComponent implements OnInit {

  csv_data_list: any[];
  error: any;
  value_error: any;
  flag: boolean;
  single_value: any;

  constructor(private csvReaderService: CsvReaderService) { }
  
  @ViewChild('f') keyValueForm: NgForm;

  getCsvData() {
    this.csvReaderService
        .getData()  
        .subscribe(
        (response: Response) => {
          this.csv_data_list = response.json()
        },
        (error) => console.log(error),
      );
  }

  getValue() {
      this.csvReaderService.getSingleValue(this.keyValueForm.value)
      .subscribe(
        (response:Response) => {
          const response_data = response.json();
          this.single_value = "Key is: " + response_data[0]['Key'];
          this.flag = true;
        },
        (error:Response) => {
          this.flag = false;
          const error_msg = error.json();
          this.value_error = "Error: " + error_msg['error'];
        },
      );
  }

  ngOnInit() {
    this.getCsvData();
  }
}
