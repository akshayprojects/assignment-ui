import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { Http, Response } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { CsvreaderComponent } from './csvreader/csvreader.component';
import { CsvReaderService } from './services/csvReaderService';
@NgModule({
  declarations: [
    AppComponent,
    CsvreaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
	HttpModule,
    HttpClientModule,

  ],
  providers: [CsvReaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
