import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CsvReaderService {
  private apiURL = 'http://127.0.0.1:8000/';

  constructor(private http: Http) { }

  getData() {
    return this.http.get(this.apiURL);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  getSingleValue(keyDetails: any[]) {
    return this.http.get('http://127.0.0.1:8000/'+keyDetails['key']);
  }

}